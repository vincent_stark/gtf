=== What is it for?

Looks up Google Instant for the most common words (e.g. Oxford 3000) and then
provides data both in real-time and in a historical perspective.


=== What it can do?

* Words
** Search for trending words
** Get full list of words
** View number of searches for each of word
** Show a diagram of how words positions change over time
* Websites
** Get a list of trending sites, arranged by the number of searches
** Show a diagram of how sites positions change over time


== TODO
* Use trending words from Google itself: http://www.google.com/insights/search/#cat=0-13-302&date=today%207-d&cmpt=q
* Get stats from different sources on websites mentioned
